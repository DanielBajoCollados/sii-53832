//logger.cpp
//Daniel Bajo Collados
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]){
	//Creacion del FIFO
	if (mkfifo ("/tmp/FIFOlog", 0666)!=0){
		perror("No puede crearse el FIFO en logger.cpp");
		return (1);
	}
	
	//Apertura del FIFO
	int fd = open("/tmp/FIFOlog", O_RDONLY);
	if (fd == -1){
		perror("No puede abrirse el FIFO en logger.cpp");
		return (1);
	}
	
	//Lectura del FIFO
	char ch;
	while (read(fd,&ch,1)>0)
		write (1, &ch, 1);
	
	//Cerrado de la FIFO
	close(fd);
	unlink("/tmp/FIFOlog");
	return 0;

}
