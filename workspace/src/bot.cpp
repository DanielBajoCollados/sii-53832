#include "DatosMemCompartida.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>


int main(){
	DatosMemCompartida* datosJ;
	int fd=open("DAT", O_RDWR, 0777);
	if (fd == -1){
		perror("No pudo abrirse el fichero de datos compartidos en bot.cpp");
	}
	datosJ = (DatosMemCompartida*)mmap(NULL, sizeof(datosJ), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);
	
	bool condition=1;
	while(condition){
		usleep(25);
		condition = (datosJ->accion!=2);
		if (datosJ->esfera.centro.x>0){//Solo se mueve si la pelota esta en su campo
			if(datosJ->esfera.centro.y<datosJ->raqueta1.y1)//Si la pelota esta por debajo del limite inferior de la raqueta, entonces se mueve hacia abajo
				datosJ->accion=-1;
			if(datosJ->esfera.centro.y>datosJ->raqueta1.y2)//Si la pelote esta por encima del limite superior de la raqueta, entonces se mueve hacia arriba
				datosJ->accion=1;
		}
	}
}
