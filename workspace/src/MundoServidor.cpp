// Mundo.cpp: implementation of the CMundoServidor class.
// Daniel Bajo Collados
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>


void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
      return d;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(FIFOdes);
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{		
	int i;
	std::string str = "Jugador X anota 1 punto, lleva N";
	char ch='\n';
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	//Jugador 2 anota punto
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio=0.5;//Al volver al centro la esfera recupera su tamanyo ariginal
		puntos2++;
		//Escritura de la FIFO
		str[8]='2';
		str[31]=(char)(puntos2+'0');
		for (int i=0; i<=sizeof(str); i++)
			if(write(FIFOdes, &str[i], 1)<1){
				perror ("Error al escribir al marcar 2");
				exit (-1);
			}
		if(write(FIFOdes, &ch, 1)<1){
				perror ("Error al escribir barra n al marcar 2");
				exit (-1);
			}
		//Se llega al maximo de puntos (3)
		if (puntos2>=3){
			str="El jugador 2 ha ganado la partida";
			puntos2 = puntos1 = 0;
			for (int i=0; i<=sizeof(str); i++)
				if (write(FIFOdes, &str[i], 1)<1){
					perror ("Error al escribir al ganar 2");
					exit (-1);
			}
			if (write(FIFOdes, &ch, 1)<1){
				perror ("Error al escribir barra n al ganar 2");
				exit (-1);
			}
		}
		
	}

	//Jugador 1 anota punto
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio=0.5;//Al volver al centro la esfera recupera su tamanyo ariginal
		puntos1++;
		//Escritura de la FIFO
		str[8]='1';
		str[sizeof(str)-1]=(char)(puntos1+'0');
		for (int i=0; i<=sizeof(str); i++)
			if (write(FIFOdes, &str[i], 1)<1){
				perror ("Error al escribir al marcar 1");
				exit (-1);
			}
		if (write(FIFOdes, &ch, 1)<1){
				perror ("Error al escribir barra n al marcar 1");
				exit (-1);
			}
		//Se llega al maximo de puntos (3)
		if (puntos1>=3){
			str="El jugador 1 ha ganado la partida";
			puntos2 = puntos1 = 0;
			for (int i=0; i<=sizeof(str); i++)
				if (write(FIFOdes, &str[i], 1)<1){
				perror ("Error al escribir al ganar 1");
				exit (-1);
			}
			if (write(FIFOdes, &ch, 1)<1){
				perror ("Error al escribir barra n al ganar 1");
				exit (-1);
			}
		}
	}
	
	//El tamanyo de la esfera se reduce hasta 0.1 en el transcurso de medio minuto
	if(esfera.radio>0.1)
		esfera.radio-=0.0003;

	//Envio de las coordenadas al cliente
	char cadcoord[200];
	sprintf(cadcoord, "%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
	comun.Send(cadcoord, 200);

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	default:jugador2.velocidad.y=jugador1.velocidad.y=0;

	}
}

void CMundoServidor::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	

	//Apertura del FIFO
	
	FIFOdes = open(nameFIFO, O_WRONLY|O_CREAT);
	if (FIFOdes == -1){
		perror("No pudo abrirse el FIFO en Mundo.cpp");
		exit (-1);
	}
	
	//Creacion del thread de las teclas
	int err = pthread_create(&teclasthread, NULL, hilo_comandos, this);
	if (err == -1){
		perror("No se pudo crear el thread para las teclas en MundoServidor.h");
		exit (-1);
	}
	
	//Enlace del Socket de conexion
	err = conex.InitServer("127.0.0.1", 8000);
	if (err == -1){
		perror("Error en InitServer de conex en MundoServidor.cpp");
		exit (-1);
	}
	
	//Enlace del socket de comunicacion
	comun = conex.Accept();
	
	char cad[200];
	err = comun.Receive((char*)&cad, 200);
	if (err == -1){
		perror("Error al recibir el nombre del cliente en MundoServidor.cpp");
		exit (-1);
	}
	write(1, &cad, 200);//Se imprime el nombre del cliente por pantalla
	
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad;
            comun.Receive(&cad, 1);
            unsigned char key;
            sscanf(&cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
