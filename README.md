# Tenis
Tenis es un juego similar al Pong, en el cual dos jugadores mueven unas raquetas para que marcarle puntos al contrario con una pelota.

# Modo de juego
Para jugar con el jugador 1 (derecha) se usan las teclas 'w' y 's' para moverse hacia arriba y hacia abajo respectivamente, y para mover al jugador 2 (izquierda) se usan 'o' y 'l'.
Es posible abrir otra ventana para poder jugar desde dos equipos.
También es posible controlar al jugador 2 mediante un bot.

# Autores y reconocimientos
El juego se ha hecho por Daniel Bajo Collados a partir de los archivos y guiones de las prácticas de Sistemas Informáticos Industriales de la ETSIDI, hechos por Raquel Cedazo y Angel Rodríguez.

# Estatus del proyecto
El juego se ha terminado.
