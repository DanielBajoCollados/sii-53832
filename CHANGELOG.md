# Cahngelog
Todos los cambios realizados en el proyecto se documentarán en este archivo
Este Changelog usa Semantic Versioning

## [sin publicar]

##[5.0.0] - 2020-12-28
### Modificado
-Ahora la comunicación entre cliente y servidor se hace mediante Sockets, en lugar de tuberías con nombre. Para ello se ha hecho uso de la clase Socket.
-Se ha modificado el archivo Socket.cpp para evitar el uso de la librería <iostream>, ya que las funciones cout, cin y cerr de std daban errores. En su lugar se han usado printf, scanf y perror respectivamente.
### Eliminado
-Se han eliminado las tuberías que comunicaban al cliente y al servidor entre sí.

## [4.0.0] - 2020-12-16
### Añadido
-Se han creado cliente y servidor y se ha realizado la comunicación entre los mismos.
### Eliminado
-Se ha eliminado tenis, tenis.cpp y Mundo.cpp.
### Modificado
-Logger ahora se comunica con el servidor y bot con el cliente.
### Arreglado
-Ahora sí salen los mensajes por logger, en lugar de por el servidor, anteriormente tenis.

## [3.0.0] - 2020-12-02
### Añadido
-Ahora la puntuación aparece en el terminal.
-También se ha implementado un bot que controla al jugador 2.
-Por último, se ha añadido un proceso por el cual el primer jugador que obtiene 3 puntos gana el partido, causando que la partida se reinicie.

## [2.0.0] - 2020-10-29
### Añadido
-Ahora se pueden mover tanto la esfera como las raquetas. Además, la esfera va reduciendo su tamaño hasta un quinto del original. Una vez la esfera llega a una portería recupera su tamaño original


## [1.1.0] - 2020-10-15
### Añadido
-En las cabeceras de todos los ficheros .cpp del directorio /workspace/src mi nombre "Daniel Bajo Collados", tal y como se indica en la práctica.
